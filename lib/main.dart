import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'dart:convert' show json, jsonDecode;
import 'dart:async';
import 'dart:core';
import 'dart:ui' as ui;
import 'package:firebase_core/firebase_core.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:audiofileplayer/audiofileplayer.dart';
import 'package:flutter/services.dart';
import 'package:flare_splash_screen/flare_splash_screen.dart';

//import 'package:get/get.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter/widgets.dart';

import 'package:google_sign_in/google_sign_in.dart';
import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:flutter_facebook_login/flutter_facebook_login.dart';

import 'package:flutter_facebook_auth/flutter_facebook_auth.dart';

import "package:http/http.dart" as http;

import 'package:flutter/foundation.dart';
import 'package:rive/rive.dart';

//import 'package:animated_splash_screen/animated_splash_screen.dart';
import 'package:flare_splash_screen/flare_splash_screen.dart';

//import 'package:flutter_audio_player/flutter_audio_player.dart';
//import 'package:audiofileplayer/audiofileplayer.dart';

import 'dart:math';
import 'info.dart';
import 'package:shared_preferences/shared_preferences.dart';

import 'dart:io';

import 'package:flutter/services.dart';
//import 'package:animated_splash/animated_splash.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';

//import 'package:in_app_purchase_android/in_app_purchase_android.dart';
import 'package:flutter/foundation.dart';

import 'dart:convert';

import 'package:flutter/foundation.dart';

import 'package:flutter_local_notifications/flutter_local_notifications.dart';

import 'package:flutter_inapp_purchase/flutter_inapp_purchase.dart';

//const Set<String> _kIds = {'credits_count'};
//final String testID = 'credits_count';
final FirebaseAuth _auth = FirebaseAuth.instance;
int p = 0;
int _KiWebinterface = 0;
String theemail = "";

Future<void> _firebaseMessagingBackgroundHandler(RemoteMessage message) async {
  AlertDialog(
    title: Text(""),
    content: Text("Login to continue"),
  );
  // If you're going to use other Firebase services in the background, such as Firestore,
  // make sure you call `initializeApp` before using other Firebase services.
  await Firebase.initializeApp();

  RemoteNotification notification = message.notification;
  AndroidNotification android = message.notification?.android;
  if (message.data['matchid'].notnull) {
    p = 1234;
    runApp(info());
    print(message.data['matchid']);
  } else {
    print("and");
//  showAlert2(context, message.data['fcm'],message.data['email'],message.data['name']);
  }

  print('Handling a background message ${message.messageId}');
}

/// Create a [AndroidNotificationChannel] for heads up notifications
AndroidNotificationChannel channel;

/// Initialize the [FlutterLocalNotificationsPlugin] package.
FlutterLocalNotificationsPlugin flutterLocalNotificationsPlugin;

GoogleSignIn _googleSignIn = GoogleSignIn(
  // Optional clientId
  // clientId: '479882132969-9i9aqik3jfjd7qhci1nqf0bm2g71rm1u.apps.googleusercontent.com',
  scopes: <String>[
    'email',
    // 'https://www.googleapis.com/auth/contacts.readonly',
  ],
);

void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  await Firebase.initializeApp();

  //await FirebaseAuth.instance.useEmulator('localhost');
  FirebaseMessaging.onBackgroundMessage(_firebaseMessagingBackgroundHandler);

  if (!kIsWeb) {
    channel = const AndroidNotificationChannel(
      'high_importance_channel', // id
      'High Importance Notifications', // title
      'This channel is used for important notifications.', // description
      importance: Importance.high,
    );

    flutterLocalNotificationsPlugin = FlutterLocalNotificationsPlugin();

    /// Create an Android Notification Channel.
    ///
    /// We use this channel in the `AndroidManifest.xml` file to override the
    /// default FCM channel to enable heads up notifications.
    await flutterLocalNotificationsPlugin
        .resolvePlatformSpecificImplementation<
            AndroidFlutterLocalNotificationsPlugin>()
        ?.createNotificationChannel(channel);

    await FirebaseMessaging.instance
        .setForegroundNotificationPresentationOptions(
      alert: true,
      badge: true,
      sound: true,
    );
  }

  SystemChrome.setPreferredOrientations(
      [DeviceOrientation.portraitUp, DeviceOrientation.portraitDown]);

  if (p != 1234) {
    runApp(MyApp());
  }
}

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Draw with friends',
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      home: MyHomePage(title: 'Home Page'),
    );
  }
}

class MyHomePage extends StatefulWidget {
  MyHomePage({Key key, this.title}) : super(key: key);
  final String title;

  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage>
    with SingleTickerProviderStateMixin {
  static final FacebookLogin facebookSignIn = new FacebookLogin();

  //InAppPurchase iap=InAppPurchase.instance;

  StreamSubscription _purchaseUpdatedSubscription;
  StreamSubscription _purchaseErrorSubscription;
  StreamSubscription _conectionSubscription;
  final List<String> _productLists = Platform.isAndroid
      ? [
          'credits_count',
        ]
      : ['com.muhammadzubair.Credits'];

  String _platformVersion = 'Android';
  List<IAPItem> _items = [];
  List<PurchasedItem> _purchases = [];

  //final TextEditingController _emailController = TextEditingController();
  //final TextEditingController _passwordController = TextEditingController();
  GoogleSignInAccount _currentUser;
  String _contactText = '';

  //GoogleSignIn _googleSignIn;
  //GoogleSignInAccount _currentUser;

  GlobalKey globalKey = GlobalKey();
  GlobalKey globalKey1 = GlobalKey();
  GlobalKey globalKey2 = GlobalKey();
  GlobalKey globalKey4 = GlobalKey();
  String string34 = "Please Singin...";
  String stringdp = "";
  String _token;

  // Audio audio;
  //String theemail="";
  String iname = "";
  String iid = "";
  String ifcm = "";
  String iemail = "";
  String idp = "";
  String messageTitle = "Empty";
  String notificationAlert = "alert";
//  Animation<double> animation;
//  Animation<Offset> animation1;
//  Animation<Offset> animation2;
//  Animation<Offset> animation3;
//  AnimationController controller;
//  String _currentAnimationName = "Animation 1";

//  Audio audio = Audio.load('assets/audio/pencil1.m4a');
  FirebaseMessaging _firebaseMessaging = FirebaseMessaging.instance;

  String verificationId;
  bool isLoading = false;
  FirebaseAuth auth = FirebaseAuth.instance ?? "";

  final myController = TextEditingController();

  Future getUserContacts() async {
    final host = "https://people.googleapis.com";
    final endPoint = "/v1/people/me/connections?personFields=names";
    final header = await _currentUser.authHeaders;

    setState(() {
      isLoading = true;
    });

    print("loading contact");
    final request =
        await http.get(Uri.parse("$host$endPoint"), headers: header);
    print("Loading completed");
    setState(() {
      isLoading = false;
    });

    if (request.statusCode == 200) {
      print("Api working perfect");
      print(request.body);
    } else {
      print("Api got error");
      print(request.body);
    }
  }

  @override
  void dispose() {
    if (_conectionSubscription != null) {
      _conectionSubscription.cancel();
      _conectionSubscription = null;
    }

    //  _subscription.cancel();
    super.dispose();
    // TODO: implement dispose
    //
  }

  @override
  void initState() {
    super.initState();
    initPlatformState();

    //  showAlert(context, "adsgvhdjfkls;a");

    FirebaseMessaging.instance
        .getInitialMessage()
        .then((RemoteMessage message) {
      if (message != null) {
        Navigator.pushNamed(
          context,
          '/message',
        );
      }
    });

    FirebaseMessaging.onMessage.listen((RemoteMessage message) {
      RemoteNotification notification = message.notification;
      AndroidNotification android = message.notification?.android;
      print("message is");
      //print(message.data['matchid']);
      //print(message.data['matchid']);
      print("handling notificatiooooo");
      print("matchidis");
      print(message.data['matchid']);
      print(message);
      print("so");
      print(message.data);
      print("matchidis");
      print(message.data['matchid']);
      if (message.data['matchid'] != "a") {
        print("show alert");

        print(message.data['matchid']);
        print(context);
        //   Get.defaultDialog(title:"Alert");

        showAlert(context, message.data['matchid'], iemail);
      } else {
        print("show alert 2");
        showAlert2(context, message.data['email']);
      }

      if (notification != null && android != null && !kIsWeb) {
        flutterLocalNotificationsPlugin.show(
            notification.hashCode,
            notification.title,
            notification.body,
            NotificationDetails(
              android: AndroidNotificationDetails(
                channel.id,
                channel.name,
                channel.description,
                // TODO add a proper drawable resource to android, for now using
                //      one that already exists in example app.
                icon: 'launch_background',
              ),
            ));
      }
    });

    FirebaseMessaging.onMessageOpenedApp.listen((RemoteMessage message) {
      print('NEW');
      print("handling notification");
      print(message.data);
      print("matchidis");
      print(message.data['matchid']);
      if (message.data['matchid'] != "a") {
        //Get.defaultDialog(title:"Alert");
        print(context);
        showAlert(context, message.data['matchid'], iemail);
      } else {
        showAlert2(context, message.data['email']);
      }
    });

    _googleSignIn.onCurrentUserChanged.listen((GoogleSignInAccount account) {
      setState(() {
        _currentUser = account ?? "";
      });

      if (_currentUser != null) {
        //_handleGetContact(_currentUser);
        theemail = _currentUser.displayName ?? "";
        iemail = _currentUser.email ?? "";
        idp = _currentUser.photoUrl ?? "";
        iname = _currentUser.displayName ?? "";
        setState(() {
          string34 = "Welcome $theemail";
          stringdp = _currentUser.photoUrl ?? "";
        });
        FirebaseAuth.instance.userChanges().listen((User user) {
          if (user == null) {
            print('User is currently signed out!');
            _handleSignIn();
          } else {
            print('User is signed in!');
          }
        });
        // getUserContacts();
      }
    });
    reademail();
    print(theemail);

    _googleSignIn.signInSilently();
    getToken();
/*
    if (theemail.length>6)
      {Future.delayed(Duration.zero, () {
        showAlertn(context);
      });
          }
    else
      {
       Future.delayed(Duration.zero, () {
          showAlert(context);
        });

      }
*/
  }

  // Platform messages are asynchronous, so we initialize in an async method.
  Future<void> initPlatformState() async {
    String platformVersion;
    // Platform messages may fail, so we use a try/catch PlatformException.
    try {
      platformVersion = await FlutterInappPurchase.instance.platformVersion;
    } on PlatformException {
      platformVersion = 'Failed to get platform version.';
    }

    // prepare
    var result = await FlutterInappPurchase.instance.initConnection;
    print('result: $result');

    // If the widget was removed from the tree while the asynchronous platform
    // message was in flight, we want to discard the reply rather than calling
    // setState to update our non-existent appearance.
    if (!mounted) return;

    setState(() {
      _platformVersion = platformVersion;
    });

    // refresh items for android
    try {
      String msg = await FlutterInappPurchase.instance.consumeAllItems;
      print('consumeAllItems: $msg');
    } catch (err) {
      print('consumeAllItems error: $err');
    }

    _conectionSubscription =
        FlutterInappPurchase.connectionUpdated.listen((connected) {
      print('connected: $connected');
    });

    _purchaseUpdatedSubscription =
        FlutterInappPurchase.purchaseUpdated.listen((productItem) {
      print('purchase-updated: $productItem');
    });

    _purchaseErrorSubscription =
        FlutterInappPurchase.purchaseError.listen((purchaseError) {
      print('purchase-error: $purchaseError');
    });
  }

  void _requestPurchase(IAPItem item) {
    FlutterInappPurchase.instance.requestPurchase(item.productId);
  }

  Future _getProduct() async {
    List<IAPItem> items =
        await FlutterInappPurchase.instance.getProducts(_productLists);
    for (var item in items) {
      print('${item.toString()}');
      this._items.add(item);
    }

    setState(() {
      this._items = items;
      this._purchases = [];
    });
  }

  Future _getPurchases() async {
    List<PurchasedItem> items =
        await FlutterInappPurchase.instance.getAvailablePurchases();
    for (var item in items) {
      print('${item.toString()}');
      this._purchases.add(item);
    }

    setState(() {
      this._items = [];
      this._purchases = items;
    });
  }

  Future _getPurchaseHistory() async {
    List<PurchasedItem> items =
        await FlutterInappPurchase.instance.getPurchaseHistory();
    for (var item in items) {
      print('${item.toString()}');
      this._purchases.add(item);
    }

    setState(() {
      this._items = [];
      this._purchases = items;
    });
  }

  List<Widget> _renderInApps() {
    List<Widget> widgets = this
        ._items
        .map((item) => Container(
              margin: EdgeInsets.symmetric(vertical: 10.0),
              child: Container(
                child: Column(
                  children: <Widget>[
                    Container(
                      margin: EdgeInsets.only(bottom: 5.0),
                      child: Text(
                        item.toString(),
                        style: TextStyle(
                          fontSize: 18.0,
                          color: Colors.black,
                        ),
                      ),
                    ),
                    FlatButton(
                      color: Colors.orange,
                      onPressed: () {
                        print("---------- Buy Item Button Pressed");
                        this._requestPurchase(item);
                      },
                      child: Row(
                        children: <Widget>[
                          Expanded(
                            child: Container(
                              height: 48.0,
                              alignment: Alignment(-1.0, 0.0),
                              child: Text('Buy Item'),
                            ),
                          ),
                        ],
                      ),
                    ),
                  ],
                ),
              ),
            ))
        .toList();
    return widgets;
  }

  List<Widget> _renderPurchases() {
    List<Widget> widgets = this
        ._purchases
        .map((item) => Container(
              margin: EdgeInsets.symmetric(vertical: 10.0),
              child: Container(
                child: Column(
                  children: <Widget>[
                    Container(
                      margin: EdgeInsets.only(bottom: 5.0),
                      child: Text(
                        item.toString(),
                        style: TextStyle(
                          fontSize: 18.0,
                          color: Colors.black,
                        ),
                      ),
                    )
                  ],
                ),
              ),
            ))
        .toList();
    return widgets;
  }

  reademail() async {
    final prefs = await SharedPreferences.getInstance();
    //  var vol= prefs.getString('volume') ?? "5";

    // double _value=double.parse(vol);
    // audio.setVolume(_value);

// Try reading data from the counter key. If it doesn't exist, return 0.
    theemail = prefs.getString('theemail') ?? "";

    iemail = prefs.getString('theemail') ?? "";
    idp = prefs.getString('dp') ?? "";
    iname = prefs.getString('name') ?? "";
    if (stringdp.length < 5) {
      setState(() {
        string34 = prefs.getString('name') ?? "Please Login..";
        stringdp = prefs.getString('dp') ?? "";
      });
    }
    print("chaack3");
  }

  Future<void> _handleSignIn() async {
    try {
      var user = _googleSignIn.signIn().then((result) {
        result.authentication.then((googleKey) async {
          print(googleKey.accessToken);
          print(googleKey.idToken);
          print(_googleSignIn.currentUser.displayName);
          final credential = GoogleAuthProvider.credential(
            accessToken: googleKey.accessToken,
            idToken: googleKey.idToken,
          );
          await FirebaseAuth.instance.signInWithCredential(credential);
          ScaffoldMessenger.of(context).showSnackBar(const SnackBar(
            content: Text('Logged in'),
          ));
        }).catchError((err) {
          print('inner error');
          print("ghalti4");
        });
      }).catchError((err) {
        print('error occured');
        print("ghalti5");
      });

      print(_currentUser.displayName);
      print(_currentUser.email);
      print(_currentUser.id);
      print(_currentUser.photoUrl);

      final prefs = await SharedPreferences.getInstance();

// set value
      prefs.setString('theemail', _currentUser.email);
      prefs.setString('name', _currentUser.displayName);
      prefs.setString('dp', _currentUser.photoUrl);
      theemail = _currentUser.email;
      FirebaseAuth.instance.userChanges().listen((User user) {
        if (user == null) {
          print('User is currently signed out!');
        } else {
          print('User is signed in!');
        }
      });
    } catch (error) {
      print("ghalti6");
      print(error);
    }
    setState(
      () {
        Navigator.of(context).popUntil((route) => route.isFirst);
        ;
      },
    );
  }

  Future<void> _handleSignOut() async {
    //await _googleSignIn.disconnect();
    await _googleSignIn.signOut();
    print("rt1");
    await facebookSignIn.logOut();
    print("rt2");
    final prefs = await SharedPreferences.getInstance();
    print("rt3");

// set value
    prefs.setString('theemail', '');
    prefs.setString('name', '');
    prefs.setString('dp', '');
    showMessage(context, "Logged out");
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Stack(children: [
        Container(
          color: Colors.black, //yellow[100],
          width: MediaQuery.of(context).size.width,
          height: MediaQuery.of(context).size.height,
          child: Image.asset('assets/pics/s10.png', fit: BoxFit.fitHeight),
        ),
        Container(
          width: MediaQuery.of(context).size.width,
          height: MediaQuery.of(context).size.height,
          padding: EdgeInsets.only(left: 5, top: 10, right: 0, bottom: 0),
          color: Colors.transparent,
          child: Column(
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            children: [
              SizedBox(
                height: 20,
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.end,
                children: [
                  Image.asset(
                    'assets/pics/coins.png',
                    width: 40,
                    height: 40,
                  ),
                  Text(
                    ' $_KiWebinterface   ',
                    style: TextStyle(
                        fontSize: 20,
                        foreground: Paint()
                          ..shader = ui.Gradient.linear(
                            const Offset(0, 10),
                            const Offset(250, 10),
                            <Color>[
                              Colors.yellowAccent,
                              Colors.yellow,
                            ],
                          )),
                  ),
                ],
              ),
              Row(
                children: [
                  RepaintBoundary.wrap(btn22(), 0),
                  Column(children: [
                    RepaintBoundary.wrap(btn12(), 0),
                  ]),
                ],
              ),
              Spacer(),
              SizedBox(
                height: 140,

                // width: MediaQuery.of(context).size.width-260,
                // <-- Your width

                child: Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    GestureDetector(
                      onTap: () {
                        showAlert1(context);
                      },
                      child: Container(
                        // color: Colors.white54,
                        decoration: BoxDecoration(
                          color: Colors.transparent,
                          borderRadius: BorderRadius.only(
                              topLeft: Radius.circular(140.0),
                              // bottomLeft: Radius.circular(120.0),
                              // bottomRight: Radius.circular(120.0),
                              topRight: Radius.circular(140.0)),
                        ),
                        height: 140,
                        alignment: Alignment.center,
                        width: MediaQuery.of(context).size.width - 60,
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            //   SizedBox(width:MediaQuery.of(context).size.width/2-70,),
                            Image.asset(
                              'assets/pics/si.png',
                              width: 140,
                              height: 140,
                            ),
                            /*    SizedBox(width:10.0,),
                        Text( "Sign in",
                          style: TextStyle(color: Colors.yellowAccent,),
                          textAlign: TextAlign.center,
                          textScaleFactor: 3,

                        ),
*/
                          ],
                        ),
                      ),
                    ),
                  ],
                ),
              ),
              Spacer(),
              SizedBox(
                height: 180,
                //width: MediaQuery.of(context).size.width-200, // <-- Your width
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    GestureDetector(
                      onTap: () {
                        setState(
                          () {
                            Navigator.push(
                              context,
                              MaterialPageRoute<void>(
                                builder: (BuildContext context) => info(),
                              ),
                            );
                          },
                        );
                      },
                      child: Container(
                        color: Colors.transparent,
                        height: 180,

                        width: MediaQuery.of(context).size.width - 90,
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            //    SizedBox(width:30.0,),
                            Image.asset(
                              'assets/pics/sp.png',
                              width: 180,
                              height: 180,
                            ),
                            /*   SizedBox(width:10.0,),
                               Text( "Play",
                                  style: TextStyle(color: Colors.purpleAccent[200],),
                                  textAlign: TextAlign.center,
                                  textScaleFactor: 7,

                                ),*/
                          ],
                        ),

                        // height: double.infinity,
                        // width: double.infinity,),),),
                      ),
                    ),
                  ],
                ),
              ),
              Spacer(),
              SizedBox(
                height: 140,
                //width: MediaQuery.of(context).size.width-150, // <-- Your width
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    GestureDetector(
                      onTap: () {
                        /*  setState(() {
                              Navigator.push(
                                context,
                                MaterialPageRoute<void>(

                                  builder: (BuildContext context) =>
                                      info(),
                              );
                            },
                            );*/
                        showAlerta(context);
                      },
                      child: Container(
                        //color: Colors.amber[300],
                        decoration: BoxDecoration(
                            color: Colors.transparent,
                            borderRadius: BorderRadius.only(
                                bottomLeft: Radius.circular(140.0),
                                bottomRight: Radius.circular(140.0))),
                        alignment: Alignment.center,
                        height: 140,
                        width: MediaQuery.of(context).size.width - 60,
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            //  SizedBox(width:20.0,),
                            Image.asset(
                              'assets/pics/mp.png',
                              width: 140,
                              height: 140,
                            ),
                            /*  SizedBox(width:10.0,),
                    Text( "Multiplayer",
                      style: TextStyle(color: Colors.pinkAccent[100],),
                      textAlign: TextAlign.center,
                      textScaleFactor: 3,

                    ),*/
                          ],
                        ),

                        // height: double.infinity,
                        // width: double.infinity,),),),
                      ),
                    ),
                  ],
                ),
              ),
              Spacer(),
              /*   GestureDetector(
                onTap: () {
                  print("fjh");
                  showAlert3(context);
                }, child:  Container(
                height: 220,
                width:340,
                alignment: Alignment.bottomRight,
                child:Image.asset('assets/pics/setting.png'),
                // height: double.infinity,
                // width: double.infinity,),),),

              ),),*/
              /*    SizedBox(
                height: 70,
               // width: MediaQuery.of(context).size.width-110, // <-- Your width
                child:Row(
                  mainAxisAlignment: MainAxisAlignment.end,
                  children: [
                  GestureDetector(
                    onTap: () {
                      setState(() {
                        Navigator.push(
                          context,
                          MaterialPageRoute<void>(

                            builder: (BuildContext context) => info(),

                          ),
                        );
                        // Navigator.of(context).popUntil((route) => route.isFirst);
                        print("______++++++++++++++++++_______________2");
                      },
                      );
                    }, child:Container(
                    height: 70,
                    width:200,
                    child:Image.asset('assets/pics/opponents.png'),
                    // height: double.infinity,
                    // width: double.infinity,),),),

                  ),),
                ],),),
*/
              Spacer(),
            ],
          ),
        ),
      ]),
    );
  }

  @override
  void setState(fn) {
    if (mounted) {
      super.setState(fn);
    }
  }

  void showAlert1(BuildContext context) {
    showGeneralDialog(
        context: context,
        barrierDismissible: true,
        barrierLabel:
            MaterialLocalizations.of(context).modalBarrierDismissLabel,
        barrierColor: Colors.black45,
        transitionDuration: const Duration(milliseconds: 200),
        pageBuilder: (BuildContext buildContext, Animation animation,
            Animation secondaryAnimation) {
          return Container(
            height: MediaQuery.of(context).size.height,
            width: MediaQuery.of(context).size.width,
            color: Colors.blueGrey,
            child: Stack(
              fit: StackFit.expand,
              children: [
                Image.asset('assets/pics/s9.png', fit: BoxFit.fitHeight),
                Container(
                  child: Column(
                    children: [
                      Spacer(),
                      SizedBox(
                        height: 140,
                        width: MediaQuery.of(context).size.width -
                            50, // <-- Your width
                        child: GestureDetector(
                          onTap: () {
                            _handleSignIn();
                          },
                          child: Container(
                            height: 140,
                            width: 140,
                            child: Image.asset('assets/pics/G.png'),
                            // height: double.infinity,
                            // width: double.infinity,),),),
                          ),
                        ),
                      ),
                      Spacer(),
                      SizedBox(
                        height: 140,
                        width: MediaQuery.of(context).size.width -
                            50, // <-- Your width
                        child: GestureDetector(
                          onTap: () {
                            _login();
                          },
                          child: Container(
                            height: 140,
                            width: 140,
                            child: Image.asset('assets/pics/F.png'),
                            // height: double.infinity,
                            // width: double.infinity,),),),
                          ),
                        ),
                      ),
                      //TextButton(onPressed: () async {phonea();}, child: Text("Phone")),

                      Spacer(),
                      SizedBox(
                        height: 140,
                        width: MediaQuery.of(context).size.width -
                            50, // <-- Your width
                        child: GestureDetector(
                          onTap: () {
                            _handleSignOut();
                          },
                          child: Container(
                            height: 140,
                            width: 140,
                            child: Image.asset('assets/pics/signout.png'),
                            // height: double.infinity,
                            // width: double.infinity,),),),
                          ),
                        ),
                      ),
                      Spacer(),
                      /* SizedBox(
                    height: 100,
                    width: MediaQuery.of(context).size.width-50, // <-- Your width
                    child: TextButton(onPressed:() {
                     setState(() {
                            Navigator.pop(context);
                            });
                    }, child: Text("Back"),
                      style: ButtonStyle(
                        foregroundColor: MaterialStateProperty.all<Color>(Colors.yellow),
                        backgroundColor: MaterialStateProperty.all<Color>(Colors.black12),
                      ),
                    ),),*/
                      FlatButton(
                        onPressed: () {
                          setState(() {
                            Navigator.pop(context);
                          });
                          // Add your onPressed code here!
                        },
                        child: Image.asset(
                          "assets/pics/back.png",
                          width: 100,
                          height: 40,
                        ),
                      ),
                      Spacer(),
                    ],
                  ),
                ),
              ],
            ),
          );
        });
  }

  Future<void> showAlert2(BuildContext context, String notifemail) async {
    print("chaack2");
    final firestore = FirebaseFirestore.instance;
    var ds = await firestore.collection('info').doc(notifemail).get();
    print(ds);
    print("Sahi");

    String notiffcm = ds["fcm"];
    String notifid = ds["uid"];
    String notifdp = ds["dp"];
    String notifname = ds["name"];
    showGeneralDialog(
        context: context,
        barrierDismissible: true,
        barrierLabel:
            MaterialLocalizations.of(context).modalBarrierDismissLabel,
        barrierColor: Colors.black45,
        transitionDuration: const Duration(milliseconds: 200),
        pageBuilder: (BuildContext buildContext, Animation animation,
            Animation secondaryAnimation) {
          return Scaffold(
            body: Center(
              child: Container(
                width: MediaQuery.of(context).size.width - 20,
                height: MediaQuery.of(context).size.width,

                //padding: EdgeInsets.only(left: 10,top: 5,right: 10,bottom: 10),
                color: Colors.lightGreenAccent,
                child: Column(
                  children: [
                    Spacer(),
                    Image.network(
                      notifdp,
                      height: 100,
                      width: 100,
                      alignment: Alignment.topLeft,
                    ),
                    Text("$notifname sent a friend request"),
                    Spacer(),
                    Row(
                      children: [
                        GestureDetector(
                          onTap: () {
                            setState(() async {
                              final firestore12 = FirebaseFirestore.instance;

                              final firestore21 = FirebaseFirestore.instance;

                              try {
                                await firestore12
                                    .collection("friendlist")
                                    .doc('l')
                                    .collection(iemail)
                                    .doc(notifname)
                                    .set({
                                  "name": notifname,
                                  "uid": notifid,
                                  "fcm": notiffcm,
                                  "email": notifemail,
                                  "dp": notifdp,
                                });
                              } catch (e) {
                                print(e);
                                print("OIOIOIOIOIOIOIOIOIOIOIOOIOIO");
                              }

                              try {
                                print("notifemail");
                                print("notifname");
                                print(notifemail);
                                print(notifname);
                                print(iemail);
                                print(iname);
                                await firestore21
                                    .collection("friendlist")
                                    .doc('l')
                                    .collection(notifemail)
                                    .doc(iname)
                                    .set({
                                  "name": iname,
                                  "uid": iid,
                                  "fcm": ifcm,
                                  "email": iemail,
                                  "dp": idp,
                                });
                              } catch (e) {
                                print(e);
                                print("OIOIOIOIOIOIOIOIOIOIOIOOIOIO");
                              }
                              Navigator.pop(context);
                            });
                          },
                          child: Container(
                            height: 50,
                            width: 100,
                            child: Image.asset('assets/pics/accept.png'),
                            // height: double.infinity,
                            // width: double.infinity,),),),
                          ),
                        ),
                        Spacer(),
                        GestureDetector(
                          onTap: () {
                            Navigator.pop(context);
                          },
                          child: Container(
                            height: 50,
                            width: 100,
                            child: Image.asset('assets/pics/reject.png'),
                            // height: double.infinity,
                            // width: double.infinity,),),),
                          ),
                        ),
                      ],
                    ),
                    Spacer(),
                  ],
                ),
              ),
            ),
          );
        });
  }

  Future<void> showAlert3(BuildContext context) async {
    print("show alert 3");
    final prefs = await SharedPreferences.getInstance();
    var vol = prefs.getString('volume') ?? "5";

    double _value = double.parse(vol);

    showGeneralDialog(
        context: context,
        barrierDismissible: true,
        barrierLabel:
            MaterialLocalizations.of(context).modalBarrierDismissLabel,
        barrierColor: Colors.black45,
        transitionDuration: const Duration(milliseconds: 200),
        pageBuilder: (BuildContext buildContext, Animation animation,
            Animation secondaryAnimation) {
          return Scaffold(
            body: Center(
              child: Column(
                children: [
                  SizedBox(
                    height: 125,
                  ),

                  SizedBox(
                    height: 25,
                  ),

                  //TextButton(onPressed: () async {phonea();}, child: Text("Phone")),

                  /* SizedBox(
                    height: 100,
                    width: MediaQuery.of(context).size.width-50, // <-- Your width
                    child: TextButton(onPressed:() {
                     setState(() {
                            Navigator.pop(context);
                            });
                    }, child: Text("Back"),
                      style: ButtonStyle(
                        foregroundColor: MaterialStateProperty.all<Color>(Colors.yellow),
                        backgroundColor: MaterialStateProperty.all<Color>(Colors.black12),
                      ),
                    ),),*/
                  FlatButton(
                    onPressed: () {
                      setState(() {
                        Navigator.pop(context);
                      });
                      // Add your onPressed code here!
                    },

                    child: const Icon(Icons.close),
                    //   backgroundColor: Colors.indigoAccent,
                  ),
                ],
              ),
            ),
          );
        });
  }

  void showAlertn(BuildContext context) {
    showGeneralDialog(
        context: context,
        barrierDismissible: true,
        barrierLabel:
            MaterialLocalizations.of(context).modalBarrierDismissLabel,
        barrierColor: Colors.black45,
        transitionDuration: const Duration(milliseconds: 200),
        pageBuilder: (BuildContext buildContext, Animation animation,
            Animation secondaryAnimation) {
          return Scaffold(
            body: Center(
              child: Container(
                width: MediaQuery.of(context).size.width,
                height: 300,
                padding:
                    EdgeInsets.only(left: 10, top: 5, right: 10, bottom: 10),
                color: Colors.blue,
                child: Column(
                  children: [
                    SizedBox(
                      height: 125,
                    ),
                    Row(
                      children: [
                        SizedBox(
                          height: 10,
                        ),
                        TextButton(
                            onPressed: _handleSignOut, child: Text("Signout")),
                      ],
                    ),
                    Row(
                      children: [
                        SizedBox(
                          height: 10,
                        ),
                        //TextButton(onPressed: _handleSignIn, child: Text("Google")),
                        //TextButton(onPressed: _login, child: Text("Facebook Login")),
                        //TextButton(onPressed: () async {phonea();}, child: Text("Phone")),
                        TextButton(
                            onPressed: () async {
                              fun(context);
                            },
                            child: Text("Play")),
                      ],
                    ),
                  ],
                ),
              ),
            ),
          );
        });
  }

  void showAlerta(BuildContext context) {
    showGeneralDialog(
        context: context,
        barrierDismissible: true,
        barrierLabel:
            MaterialLocalizations.of(context).modalBarrierDismissLabel,
        barrierColor: Colors.black45,
        transitionDuration: const Duration(milliseconds: 200),
        pageBuilder: (BuildContext buildContext, Animation animation,
            Animation secondaryAnimation) {
          List<Widget> stack = [];

          double screenWidth = MediaQuery.of(context).size.width - 20;
          double buttonWidth = (screenWidth / 3) - 20;

          return Container(
            padding: EdgeInsets.all(10.0),
            child: ListView(
              children: <Widget>[
                Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  mainAxisAlignment: MainAxisAlignment.start,
                  children: <Widget>[
                    Container(
                      child: Text(
                        'Running on: $_platformVersion\n',
                        style: TextStyle(fontSize: 18.0),
                      ),
                    ),
                    Column(
                      children: <Widget>[
                        Row(
                          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                          children: <Widget>[
                            Container(
                              width: buttonWidth,
                              height: 60.0,
                              margin: EdgeInsets.all(7.0),
                              child: FlatButton(
                                color: Colors.amber,
                                padding: EdgeInsets.all(0.0),
                                onPressed: () async {
                                  print(
                                      "---------- Connect Billing Button Pressed");
                                  await FlutterInappPurchase
                                      .instance.initConnection;
                                },
                                child: Container(
                                  padding:
                                      EdgeInsets.symmetric(horizontal: 20.0),
                                  alignment: Alignment(0.0, 0.0),
                                  child: Text(
                                    'Connect Billing',
                                    style: TextStyle(
                                      fontSize: 16.0,
                                    ),
                                  ),
                                ),
                              ),
                            ),
                            Container(
                              width: buttonWidth,
                              height: 60.0,
                              margin: EdgeInsets.all(7.0),
                              child: FlatButton(
                                color: Colors.amber,
                                padding: EdgeInsets.all(0.0),
                                onPressed: () async {
                                  print(
                                      "---------- End Connection Button Pressed");
                                  await FlutterInappPurchase
                                      .instance.endConnection;
                                  if (_purchaseUpdatedSubscription != null) {
                                    _purchaseUpdatedSubscription.cancel();
                                    _purchaseUpdatedSubscription = null;
                                  }
                                  if (_purchaseErrorSubscription != null) {
                                    _purchaseErrorSubscription.cancel();
                                    _purchaseErrorSubscription = null;
                                  }
                                  setState(() {
                                    this._items = [];
                                    this._purchases = [];
                                  });
                                },
                                child: Container(
                                  padding:
                                      EdgeInsets.symmetric(horizontal: 20.0),
                                  alignment: Alignment(0.0, 0.0),
                                  child: Text(
                                    'End Connection',
                                    style: TextStyle(
                                      fontSize: 16.0,
                                    ),
                                  ),
                                ),
                              ),
                            ),
                          ],
                        ),
                        Row(
                            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                            children: <Widget>[
                              Container(
                                  width: buttonWidth,
                                  height: 60.0,
                                  margin: EdgeInsets.all(7.0),
                                  child: FlatButton(
                                    color: Colors.green,
                                    padding: EdgeInsets.all(0.0),
                                    onPressed: () {
                                      print(
                                          "---------- Get Items Button Pressed");
                                      this._getProduct();
                                    },
                                    child: Container(
                                      padding: EdgeInsets.symmetric(
                                          horizontal: 20.0),
                                      alignment: Alignment(0.0, 0.0),
                                      child: Text(
                                        'Get Items',
                                        style: TextStyle(
                                          fontSize: 16.0,
                                        ),
                                      ),
                                    ),
                                  )),
                              Container(
                                  width: buttonWidth,
                                  height: 60.0,
                                  margin: EdgeInsets.all(7.0),
                                  child: FlatButton(
                                    color: Colors.green,
                                    padding: EdgeInsets.all(0.0),
                                    onPressed: () {
                                      print(
                                          "---------- Get Purchases Button Pressed");
                                      this._getPurchases();
                                    },
                                    child: Container(
                                      padding: EdgeInsets.symmetric(
                                          horizontal: 20.0),
                                      alignment: Alignment(0.0, 0.0),
                                      child: Text(
                                        'Get Purchases',
                                        style: TextStyle(
                                          fontSize: 16.0,
                                        ),
                                      ),
                                    ),
                                  )),
                              Container(
                                  width: buttonWidth,
                                  height: 60.0,
                                  margin: EdgeInsets.all(7.0),
                                  child: FlatButton(
                                    color: Colors.green,
                                    padding: EdgeInsets.all(0.0),
                                    onPressed: () {
                                      print(
                                          "---------- Get Purchase History Button Pressed");
                                      this._getPurchaseHistory();
                                    },
                                    child: Container(
                                      padding: EdgeInsets.symmetric(
                                          horizontal: 20.0),
                                      alignment: Alignment(0.0, 0.0),
                                      child: Text(
                                        'Get Purchase History',
                                        style: TextStyle(
                                          fontSize: 16.0,
                                        ),
                                      ),
                                    ),
                                  )),
                            ]),
                      ],
                    ),
                    Column(
                      children: this._renderInApps(),
                    ),
                    Column(
                      children: this._renderPurchases(),
                    ),
                  ],
                ),
              ],
            ),
          );
        });
  }

  void showAlert(BuildContext context, String mathid, String iemail) {
    showGeneralDialog(
        context: context,
        barrierDismissible: true,
        barrierLabel:
            MaterialLocalizations.of(context).modalBarrierDismissLabel,
        barrierColor: Colors.black45,
        transitionDuration: const Duration(milliseconds: 600),
        pageBuilder: (BuildContext buildContext, Animation animation,
            Animation secondaryAnimation) {
          return Scaffold(
            body: Center(
              child: Container(
                width: MediaQuery.of(context).size.width - 20,
                height: 200,

                //padding: EdgeInsets.only(left: 10,top: 5,right: 10,bottom: 10),
                color: Colors.yellowAccent,
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.end,
                  children: [
                    GestureDetector(
                      onTap: () {
                        Navigator.pushReplacement<void, void>(
                          context,
                          MaterialPageRoute<void>(
                            builder: (BuildContext context) => info(),
                          ),
                        );
                      },
                      child: Container(
                        height: 70,
                        width: 100,
                        child: Image.asset('assets/pics/accept.png'),
                        // height: double.infinity,
                        // width: double.infinity,),),),
                      ),
                    ),
                    GestureDetector(
                      onTap: () {
                        Navigator.pop(context);
                      },
                      child: Container(
                        height: 50,
                        width: 100,
                        child: Image.asset('assets/pics/reject.png'),
                        // height: double.infinity,
                        // width: double.infinity,),),),
                      ),
                    ),
                    SizedBox(
                      height: 10,
                    ),
                  ],
                ),
              ),
            ),
          );
        });
  }

  showMessage(BuildContext context, String txt) {
    return showDialog<AlertDialog>(
      context: context,
      builder: (BuildContext context) {
        return AlertDialog(
          scrollable: true,
          title: Text(txt),
          content: Padding(
            padding: const EdgeInsets.all(8.0),
            child: Column(
              children: <Widget>[
                FlatButton(
                  child: const Text('Ok'),
                  onPressed: () {
                    setState(() {
                      Navigator.pop(context);
                    });
                  },
                ),
              ],
            ),
          ),
        );
      },
    );
  }

  Future<Null> _login() async {
    final LoginResult result =
        await FacebookAuth.instance.login(permissions: ['email']);

    if (result.status == LoginStatus.success) {
      // you are logged
      final AccessToken accessToken = result.accessToken;

      final userData = await FacebookAuth.instance.getUserData();

      // signing the user to firebase using facebook
      final OAuthCredential fc =
          FacebookAuthProvider.credential(accessToken.token);

      FirebaseAuth.instance.signInWithCredential(fc);
      ScaffoldMessenger.of(context).showSnackBar(const SnackBar(
        content: Text('Logged in'),
      ));

      print(userData);
      var pictureUrl = userData['picture']['data']['url'];
      print(pictureUrl);

      String temp = userData['name'];

      setState(() {
        string34 = "Welcome $temp";
        stringdp = userData['picture']['data']['url'];
      });
      
      theemail = userData['email'];
      iemail = userData['email'];
      iname = userData['name'];
      idp = userData['dp'];

      // obtain shared preferences
      final prefs = await SharedPreferences.getInstance();

      // set value
      prefs.setString('theemail', userData['email']);
      prefs.setString('name', userData['name']);
      prefs.setString('dp', userData['picture']['data']['url']);
    } else if (result.status == LoginStatus.cancelled) {
      ScaffoldMessenger.of(context).showSnackBar(const SnackBar(
            content: Text('Login cancelled'),
          ));
      print('Login cancelled by the user.');
      // print(result.status);
      print(result.message);
    } else if (result.status == LoginStatus.failed) {
      ScaffoldMessenger.of(context).showSnackBar(const SnackBar(
            content: Text('Error signing in'),
          ));
          print('Something went wrong with the login process.\n'
              'Here\'s the error Facebook gave us: ${result.message}');
    }

    FirebaseAuth.instance.userChanges().listen((User user) {
      if (user == null) {
        print('User is currently signed out!');
      } else {
        print('User is signed in!');
      }
    });
    setState(
      () {
        Navigator.of(context).popUntil((route) => route.isFirst);
      },
    );
  }

  handleGoogleSubmit() async {
    GoogleSignInAccount googleSignInAccount = (await _googleSignIn.signIn());
    GoogleSignInAuthentication googleSignInAuthentication =
        await googleSignInAccount.authentication;
  }

  getToken() async {
    _token = await FirebaseMessaging.instance.getToken();

    print(_token);
    print(
        "________________________________________________________________________");
    //return _token;
  }

  Future<User> currentUser() async {
    final GoogleSignInAccount googleUser = (await _googleSignIn.signIn());

    final GoogleSignInAuthentication googleAuth =
        await googleUser.authentication;

    final AuthCredential credential = GoogleAuthProvider.credential(
      accessToken: googleAuth.accessToken,
      idToken: googleAuth.idToken,
    );
    print("4-IIIIIIIIIIIIOOOOOOOOOOOOIIIIIIIIIIIIIII");
    //await Firebase.initializeApp();
    final User user = (await _auth.signInWithCredential(credential)).user;
    print("5-IIIIIIIIIIIIOOOOOOOOOOOOIIIIIIIIIIIIIII");
    assert(user?.email != null);
    assert(user?.displayName != null);
    //assert(user!.isAnonymous);
    assert(await user?.getIdToken() != null);
    print("6-IIIIIIIIIIIIOOOOOOOOOOOOIIIIIIIIIIIIIII");
    final User currentUser = (await _auth.currentUser);
    print("7-IIIIIIIIIIIIOOOOOOOOOOOOIIIIIIIIIIIIIII");
    assert(user.uid == currentUser.uid);
    print("8-IIIIIIIIIIIIOOOOOOOOOOOOIIIIIIIIIIIIIII");
    print("UID  is ${user.uid}");
    return user;
  }

  Future<AlertDialog> fun(BuildContext context) {
    if (theemail.length > 6) {
      return showDialog<AlertDialog>(
        context: context,
        builder: (BuildContext context) {
          return AlertDialog(
            title: Text(""),
            content: Text("Welcome"),
            actions: <Widget>[
              FlatButton(
                child: const Text('Play'),
                onPressed: () {
                  //Navigator.of(context).popUntil((route) => route.isFirst);
                  setState(
                    () {
                      Navigator.push(
                        context,
                        MaterialPageRoute<void>(
                          builder: (BuildContext context) => info(),
                        ),
                      );
                    },
                  );
                },
              ),
              FlatButton(
                child: const Text('Invite'),
                onPressed: () {
                  //Navigator.of(context).popUntil((route) => route.isFirst);
                  print("______++++++++++++++++++_______________1");
                  setState(
                    () {
                      Navigator.push(
                        context,
                        MaterialPageRoute<void>(
                          builder: (BuildContext context) => info(),
                        ),
                      );
                      // Navigator.of(context).popUntil((route) => route.isFirst);
                      print("______++++++++++++++++++_______________2");
                    },
                  );
                },
              ),
              FlatButton(
                child: const Text('Find oppponent'),
                onPressed: () {
                  //Navigator.of(context).popUntil((route) => route.isFirst);
                  print("______++++++++++++++++++_______________1");
                  setState(
                    () {
                      Navigator.push(
                        context,
                        MaterialPageRoute<void>(
                          builder: (BuildContext context) => info(),
                        ),
                      );
                      // Navigator.of(context).popUntil((route) => route.isFirst);
                      print("______++++++++++++++++++_______________2");
                    },
                  );
                },
              ),
            ],
          );
        },
      );
    } else {
      return showDialog<AlertDialog>(
        context: context,
        builder: (BuildContext context) {
          return AlertDialog(
            title: Text(""),
            content: Text("Login to continue"),
            actions: <Widget>[
              TextButton(
                child: Text("Google"),
                onPressed: () {
                  _handleSignIn();
                  //Navigator.of(context).popUntil((route) => route.isFirst);
                },
              ),
              TextButton(
                  onPressed: () {
                    _login();
                    //Navigator.of(context).popUntil((route) => route.isFirst);
                  },
                  child: Text("Facebook Login")),
            ],
          );
        },
      );
    }
  }

  Future<AlertDialog> fun1(BuildContext context) {
    return showDialog<AlertDialog>(
      context: context,
      builder: (BuildContext context) {
        return AlertDialog(
          scrollable: true,
          title: Text('Login'),
          content: Padding(
            padding: const EdgeInsets.all(8.0),
            child: Form(
              child: Column(
                children: <Widget>[
                  TextFormField(controller: myController),
                  FlatButton(
                    child: const Text('Invite'),
                    onPressed: () {
                      setState(() {
                        Navigator.pop(context);
                      });
                    },
                  ),
                ],
              ),
            ),
          ),
        );
      },
    );
  }

  Widget btn12() {
    return Container(
      key: globalKey1,
      height: 100,
      child: Text(
        string34,
        style: TextStyle(
            fontSize: 20,
            foreground: Paint()
              ..shader = ui.Gradient.linear(
                const Offset(0, 10),
                const Offset(250, 10),
                <Color>[
                  Colors.tealAccent,
                  Colors.teal,
                ],
              )),
      ),
    );
  }

  Widget btn22() {
    return Container(
        key: globalKey2,
        height: 50,
        child: ClipRRect(
          borderRadius: BorderRadius.circular(50.0),
          child: Image.network(
            stringdp,
          ),
        ));
  }

  double transform3(double t) {
    return sin(t * pi * 2) * sin(t * pi * 2);
  }
}


/// Example implementation of the
/// [`SKPaymentQueueDelegate`](https://developer.apple.com/documentation/storekit/skpaymentqueuedelegate?language=objc).
///
/// The payment queue delegate can be implementated to provide information
/// needed to complete transactions.


